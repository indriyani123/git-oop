<!DOCTYPE html>
<html>
<head>
	<title>OOP & GIT</title>
</head>
<body>
 <h1>Belajar OOP &GIT</h1>
 <?php 

  
  require_once 'animal.php';
  require_once 'frog.php';
  require_once 'ape.php';

  $sheep = new Animalku ("shaun");


  $sungokong = new Ape("kera sakti");
  echo $sungokong->nama;
  echo "  ";
  $sungokong->yell(""); // "Auooo"
  echo "<br>";

  $kodok = new Frog("buduk");
  echo $kodok->nama;
  echo "  ";
  $kodok->jump() ; // "hop hop"
  echo "<br>";

  echo $sheep->name; // "shaun"
  echo "<br>";
  echo $sheep->legs; // 2
  echo "<br>";
  echo $sheep->cold_blooded // false


  ?>
</body>
</html>